"""
Dj URL Configuration for taestapp
"""
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from reader import views

urlpatterns = [
    url(r'^$', views.HomeView.as_view()),
    url(r'^get_author/$', views.Form.as_view(), name='get_author'),
    url(r'^thanks/$', views.ThanksView.as_view(), name='info'),
    url(r'^get_author/(?P<pk>\d+)/$', views.GetAuthorView.as_view()),
    url(r'^get_author/get_list/$', views.GetAuthorList.as_view(), name='authors_list'),
    url(r'^get_author/get_list/(?P<pk>\d+)/$', views.OneAuthorView.as_view(), name='one_author'),
    url(r'^log_in/$', views.AuthentFormView.as_view()),
    url(r'^log_out/$', views.LogOut.as_view(), name='log_out'),
    url(r'^register/$', views.RegisterFormView.as_view(), name='register'),
    url(r'^add_book/$', views.AddBookFormView.as_view(), name = 'add_book'),
    url(r'^add_reader/$', views.AddReaderFormView.as_view(), name = 'add_reader'),
    url(r'^get_readers/$', views.GetReadersListView.as_view(), name='list_of_readers'),
    url(r'^get_readers/(?P<pk>\d+)/$', views.OneReaderView.as_view(), name='one_reader'),
    url(r'^user_profile/(?P<pk>\d+)/$', views.UserProFileView.as_view(), name='user_profile'),
    url(r'^add_user_picture/(?P<pk>\d+)/$', views.UserAddProfilePictureFormView.as_view(), name='add_user_picture'),
    url(r'^user_profile/edit/(?P<pk>\d+)/$', views.UserUpdateProfileFormView.as_view(), name='user_update_profile'),
    url(r'^add_like/(?P<pk>\d+)/$', views.AddLikeView.as_view(), name='add_like'),
    url(r'^get_author/get_list/(?P<pk>\d+)/delete/$', views.AuthorDeleteView.as_view(), name='delete_author'),
    url(r'^get_readers/delete/(?P<pk>\d+)/$', views.ReaderDeleteView.as_view(), name='delete_reader'),
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
