from reader.models import Book, Author, Reader
from django.contrib.auth.models import User


def get_authors():
	author_name = Author.objects.order_by('id')[:5]
	return author_name

def get_books():
	final_dict = {}
	for name in get_authors():
		author = name.first_name[0] + '. ' + name.last_name
		temporary_list = []
		for book in Book.objects.filter(author=name):
			temporary_list.append({"name":book.name, "id":book.id, "like":book.like})
	
		final_dict[author]=temporary_list
		
	return final_dict

def get_books1():
	all_books = Book.objects.all()
	return all_books	

def get_users():
	all_readers = User.objects.all()	
	return all_readers	

def get_likes():
	all_likes = Book.objects.all()
	for l in all_likes:
		a = l.like

	return a	

def get_book_id():
	all_likes = Book.objects.all().values()

	return all_likes	

