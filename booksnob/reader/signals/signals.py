

def delete_file(sender, **kwargs):
	instance = kwargs.get('instance')
	instance.profile_image.delete(save=False)

def add_default_picture(sender, **kwargs):	
	instance = kwargs.get('instance')
	picture = UserProfile(user = User.objects.get(username = instance))
	picture.save()
		