from django.db import models
from reader.manager import AuthorManager
from django.contrib.auth.models import User
from django.db.models.signals import pre_delete, post_save
from reader.signals.signals import delete_file


class Author(models.Model):
	first_name=models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField(blank = True)
	profile_image = models.ImageField(upload_to = 'img/', verbose_name='Your photo', default = 'user_profile_img/default_image.jpeg')
	filter_author = AuthorManager
	objects = models.Manager
	
	def __unicode__(self):
		return '{0} {1}'.format(self.first_name, self.last_name)


class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)
	like = models.IntegerField(default=0)

	def __unicode__(self):
		return '{0}'.format(self.name)


class Review(models.Model):	
	book = models.ForeignKey(Book)	
	heading = models.CharField(max_length = 100, null=True)
	text = models.CharField(max_length=1000)
	amount = models.IntegerField(default=0)

	def __unicode__(self):
		return '{0} - {1}'.format(self.book.name, self.text)

class Reader(models.Model):
	book = models.ManyToManyField(Book)
	first_name = models.CharField(max_length = 50)
	last_name = models.CharField(max_length = 50)
	profile_image = models.ImageField(upload_to = 'img/', verbose_name='Your photo', default = 'without image')

	def __unicode__(self):
		return '{0} {1} {2}'.format(self.first_name, self.last_name, self.book.name)


class UserProfile(models.Model):
	user = models.ForeignKey(User)
	profile_image = models.ImageField(upload_to = 'user_profile_img/', verbose_name='Your photo', default = 'user_profile_img/default_image.jpeg')

	def __unicode__(self):
		return '{0}'.format(self.profile_image)		

#------------------signals part-------------------------

pre_delete.connect(delete_file, sender = Author)


def add_default_picture(sender, **kwargs):	
	instance = kwargs.get('instance')
	picture = UserProfile(user = User.objects.get(username = instance))
	picture.save()

post_save.connect(add_default_picture, sender = User)	
