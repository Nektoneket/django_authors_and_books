from django.contrib import admin

from reader.models import Author, Book, Reader, Review

class AuthorAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'email', 'profile_image')


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book)
admin.site.register(Reader)
admin.site.register(Review)


