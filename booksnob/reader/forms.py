from reader.models import Author, Book, Reader, UserProfile
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User

class AuthorForm(ModelForm):
	class Meta:
		model = Author
		fields = ['profile_image', 'first_name', 'last_name', 'email']


class BookForm(ModelForm):
	class Meta:
		model = Book
		fields = ['name', 'author']


class ReaderForm(ModelForm):
	class Meta:
		model = Reader
		fields = '__all__'		

class UserProfileForm(ModelForm):
	class Meta:
		model = UserProfile		
		fields = '__all__'	





			