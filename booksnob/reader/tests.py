from django.test import TestCase
from reader import models
from reader import actions

class AnimalTestCase(TestCase):
    def setUp(self):
    	self.author = models.Author.objects.create(first_name = "lion", last_name = "roar", email = "bla bla" )
        self.book = models.Book.objects.create(author = self.author, name="cat")

        
    def test_get_authors(self):
        authors = actions.get_authors()
        self.assertEqual(authors, [self.author])


    def test_get_books(self):
        books = actions.get_books()
        self.assertEqual(books, [self.book])        
        