from django.views.generic import TemplateView, DetailView, ListView, View
from reader.manager import AuthorManager
from reader import actions
from reader.forms import AuthorForm, BookForm, ReaderForm, UserProfileForm
from django.http.request import HttpRequest
from django.http import HttpResponse
from reader.models import Author
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView, UpdateView, DeleteView 
from reader.models import Book, Author, Reader, UserProfile
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import cache_page

class HomeView(TemplateView):
	template_name = 'base.html'
	
# Home page
class HomeView(TemplateView):
	template_name = 'reader/home.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView,self).get_context_data(**kwargs)
		context['author_name'] = actions.get_authors()
		context['books'] = actions.get_books()		
		context['users'] = actions.get_users()		
		return context

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class AddLikeView(View):
	def get(self,request,**kwargs ):
		book_id = kwargs['pk']
		book = Book.objects.get(id = book_id)	
		# trying to put likes in session	
		if 'like' not in request.session:			
			book.like += 1
			book.save()
			"""
			response = redirect('/')
			response.set_cookie(book_id, book_id)
			"""
			request.session['like'] = book_id 	
			print request.session['like']

			"""	
			return response
			"""
		elif 'like' in request.session:
			book.like -= 1
			book.save()
			del request.session['like']
			"""
			response = redirect('/')
			response.delete_cookie(book_id)
			return response
			"""
		return redirect('/')

	def logout(self, request):
		try:
			del request.session['like']
		except KeyError:
			pass
		return HttpResponse(" ")	


class Form(FormView):
	form_class = AuthorForm
	template_name = 'reader/get_authors.html'
	success_url = '/reader/thanks/'
	
	def form_valid(self, form):
		self.object = form.save()
		#saving image URL to the session
		self.request.session['%s_%s' % (self.object.id, self.object.first_name)] = self.object.profile_image.url		
		return super(Form, self).form_valid(form)

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class ThanksView(TemplateView):
    template_name = 'reader/thanks.html'


class GetAuthorView(DetailView):
	model = Author
	template_name = 'reader/authors.html'

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)


class OneAuthorView(DetailView):
	model = Author
	template_name = 'reader/one_author.html'
	
	def get_context_data(self, **kwargs):
		context = super(OneAuthorView,self).get_context_data(**kwargs)
		context['books_of_author'] = Book.objects.filter(author=self.object)		
		return context

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class GetAuthorList(ListView):
	model = Author	
	template_name = 'reader/get_list.html'

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)
	

class SearchAuthorList(ListView):
	model = Author	
	template_name = 'reader/input.html'
	
	def post(self,request,*args,**kwargs):
		a = request.POST.get('req', '')
		b = Author.objects.filter(first_name__startswith = a).order_by('-id')		
		print b
		return render(request, 'reader/input.html', {'author_list': b})

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class RegisterFormView(FormView):
	form_class = UserCreationForm
	template_name = 'reader/user.html'	
	success_url = '/'

	def form_valid(self,form):
		form.save()
		return super(RegisterFormView, self).form_valid(form)


class AuthentFormView(FormView):
	form_class = AuthenticationForm
	template_name = 'reader/log_in.html'	
	success_url = '/'

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)		
		return super(AuthentFormView, self).form_valid(form)		

class LogOut(View):

	def get(self,request,*args,**kwargs):
		logout(self.request)
		return redirect('/')	


class AddBookFormView(FormView):
	form_class = BookForm
	template_name = 'reader/add_book.html'
	success_url = '/reader/thanks/'

	def form_valid(self, form):
		self.object = form.save()
		return super(AddBookFormView, self).form_valid(form)
	
	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class AddReaderFormView(FormView):
	form_class = ReaderForm
	template_name = 'reader/add_reader.html'
	success_url = '/reader/thanks/'

	def form_valid(self,form):
		self.object = form.save()
		return super(AddReaderFormView, self).form_valid(form)

	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)


class GetReadersListView(ListView):

	template_name = 'reader/get_readers.html'

	def get(self,request,*args,**kwargs):
		"""
		request.session.set_expiry(60)
		"""
		request.session['pause'] = True		
		return render(request, self.template_name, {'user_list': User.objects.all()})


	
	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)	


class OneReaderView(DetailView):
	model = Reader
	template_name = 'reader/one_reader.html'
	
	@method_decorator(login_required(login_url='/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)		


class UserProFileView(DetailView):
	model = User
	template_name = 'reader/profile.html'

	def get_context_data(self, **kwargs):
		user=self.object
		context = super(UserProFileView,self).get_context_data(**kwargs)	
		context['bla'] = UserProfile.objects.filter(user=User.objects.filter(id=user.id))
		cc = []
		for i in context['bla']:
			cc.append(i.profile_image)
		context['bla'] = cc[0]	
		return  context


class UserAddProfilePictureFormView(UpdateView):
	fields = ('user','profile_image')
	model = UserProfile

	template_name = 'user_profile/add_user_picture.html'
	success_url = '/reader/thanks/'


class UserUpdateProfileFormView(UpdateView):
	fields = ('first_name','last_name','email')	
	model = User
	template_name = 'reader/edit_reader.html'
	success_url = '/reader/thanks/'


class AuthorDeleteView(DeleteView):
	model = Author
	success_url = '/reader/thanks/'

	@method_decorator(login_required(login_url='/user/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)


class ReaderDeleteView(DeleteView):
	model = User
	success_url = '/reader/thanks/'
	template_name = 'reader/user_confirm_delete.html'

	@method_decorator(login_required(login_url='/user/log_in'))
	def dispatch(self, request, *args, **kwargs):
		return super(self.__class__, self).dispatch(request, *args, **kwargs)
	
