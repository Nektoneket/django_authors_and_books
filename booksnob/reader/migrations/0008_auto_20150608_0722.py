# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0007_auto_20150608_0027'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='author',
        ),
        migrations.AlterField(
            model_name='author',
            name='profile_image',
            field=models.ImageField(default=b'without image', upload_to=b'img/', verbose_name=b'Your photo'),
        ),
    ]
