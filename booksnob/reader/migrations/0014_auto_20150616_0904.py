# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0013_auto_20150616_0433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='profile_image',
            field=models.ImageField(default=b'user_profile_img/default_image.jpeg', upload_to=b'img/', verbose_name=b'Your photo'),
        ),
    ]
