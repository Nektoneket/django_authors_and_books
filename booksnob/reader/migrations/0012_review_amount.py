# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0011_auto_20150613_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='amount',
            field=models.IntegerField(default=0),
        ),
    ]
