# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0008_auto_20150608_0722'),
    ]

    operations = [
        migrations.AddField(
            model_name='reader',
            name='profile_image',
            field=models.ImageField(default=b'without image', upload_to=b'img/', verbose_name=b'Your photo'),
        ),
    ]
