# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0010_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='like',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_image',
            field=models.ImageField(default=b'user_profile_img/default_image.jpeg', upload_to=b'user_profile_img/', verbose_name=b'Your photo'),
        ),
    ]
