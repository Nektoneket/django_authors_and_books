# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0005_review_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='profile_image',
            field=models.ImageField(default=b'media/None/no-img.jpg', upload_to=b'media/'),
        ),
        migrations.AlterField(
            model_name='author',
            name='email',
            field=models.EmailField(max_length=254, blank=True),
        ),
    ]
