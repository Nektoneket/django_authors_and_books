from critic.forms import ReviewForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView, DetailView, ListView
from reader.models import Review, Book
from critic.actions import get_reviews
from django.http.request import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render

class ReviewView(FormView):
	form_class = ReviewForm
	template_name = 'critic/review.html'
	success_url = '/critic/thanks/'

	def form_valid(self, form, *args, **kwargs):
		heading = form.cleaned_data['heading']
		text = form.cleaned_data['text']
		book_name = form.cleaned_data['book_name']	
		get_book_id = Book.objects.get(name = book_name)		
		review = Review.objects.create(
			text = text,
			heading = heading,
			book_id = get_book_id.id,)
		return super(ReviewView, self).form_valid(form)	
	

class ThanksView(TemplateView):
	template_name = 'critic/thanks.html'     

class SeeReviewsList(ListView):
	model = Review	
	template_name = 'critic/see_reviews_list.html'
  


class SeeOneReview(DetailView):
	model = Review
	template_name = 'critic/see_one_review.html'	 
