from django import forms

from reader.actions import get_books1

class ReviewForm(forms.Form):
	heading = forms.CharField(max_length = 100)
	book_name = forms.ChoiceField(choices=[(book, book) for book in get_books1()])
	text = forms.CharField(widget=forms.Textarea)

	
