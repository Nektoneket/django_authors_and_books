"""Dj URL Configuration for critic


"""
from django.conf.urls import url


from critic import views

urlpatterns = [
    url(r'^review/$', views.ReviewView.as_view(), name = 'review_book'),
    url(r'^thanks/$', views.ThanksView.as_view()),
    url(r'^see_reviews_list/$', views.SeeReviewsList.as_view(), name = 'see_reviews_list'),
    url(r'^see_reviews_list/(?P<pk>\d+)/$', views.SeeOneReview.as_view(), name = 'see_one_review'),
]
